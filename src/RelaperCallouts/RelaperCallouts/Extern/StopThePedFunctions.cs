﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

// As a special notice, you are permitted to copy the file freely without 
// following "copyleft" restrictions
// of the license, with or without modification, starting from line 15, with proper credits.

using Rage;
using StopThePed.API;

namespace RelaperCallouts.Extern
{
    internal static class StopThePedFunctions
    {
        internal static void SetVehicleHasNoRegistration(Vehicle vehicle)
        {
            if (ExternManager.StopThePedInstalled)
            {
                Functions.setVehicleRegistrationStatus(vehicle, STPVehicleStatus.None);
            }
        }

        internal static void SetPedAlcoholOverLimit(Ped ped, bool overLimit)
        {
            Functions.setPedAlcoholOverLimit(ped, overLimit);
        }

        internal static void SetPedUnderDrugsInfluence(Ped ped, bool toggle)
        {
            Functions.setPedUnderDrugsInfluence(ped, toggle);
        }
    }
}
