﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using LSPD_First_Response.Engine.Scripting.Entities;
using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Extern;
using RelaperCallouts.Util;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts
{
    [CalloutInfo("Stolen Vehicle", CalloutProbability.Medium)]
    public class StolenVehicle : CalloutBase
    {
        private Vehicle car;
        private Ped thief;
        private bool pursuing;
        private LHandle pursuit;

        protected override string Name => "Stolen Vehicle";

        protected override string ScannerCrimeName => "STOLEN_VEHICLE";
        protected override bool HasNormalCrimeAudio => true;
        protected override bool HasReportCrimeAudio => true;

        public override bool OnBeforeCalloutDisplayed()
        {
            SpawnPoint = SpawnUtil.GenerateSpawnPointAroundPlayer(300f, 600f);
            ResponseType = CalloutResponseType.Code2;
            ReportedByUnits = false;

            // Don't let it get too far away!
            this.AddMaximumDistanceCheck(650f, SpawnPoint);
            this.AddMinimumDistanceCheck(280f, SpawnPoint);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            var model = SpawnUtil.GetRandomCivilianCarModel();
            car = new Vehicle(model, SpawnPoint)
            {
                IsPersistent = true
            };

            car.Windows[0].Smash(); // so it's stolen recently
            car.DirtLevel = MathHelper.GetRandomSingle(0f, 15f); // add some dirt
            car.IsStolen = true;
            car.RandomizeLicensePlate();

            // Only if STP installed
            if (ExternManager.StopThePedInstalled && MathHelper.GetRandomInteger(0, 5) == 3)
            {
                StopThePedFunctions.SetVehicleHasNoRegistration(car);
            }

            if (ExternManager.ResponseReplaceInstalled)
            {
                thief = ResponseReplaceFunctions.CreatePed(car.Position.Around(3f));
                thief.WarpIntoVehicle(car, -1);
            }
            else
            {
                thief = car.CreateRandomDriver();
            }

            thief.IsPersistent = true;
            thief.BlockPermanentEvents = true;

            if (MathHelper.GetRandomInteger(0, 4) == 2)
            {
                Functions.AddPedContraband(thief, ContrabandType.Misc, "Brick");
            }

            Blip = car.AttachBlip();
            Blip.Sprite = BlipSprite.Enemy;
            Blip.Scale = 0.5f;
            Blip.SetColor(BlipColor.Red);
            Blip.SetRouteColor(BlipColor.Red);
            Blip.IsRouteEnabled = true;



            Functions.SetPedResistanceChance(thief, 80f);
            thief.Tasks.CruiseWithVehicle(30f);

            ScannerMessages.DisplayDispatchText("Stolen Vehicle", "Track down the ~g~vehicle~w~ and arrest the ~r~suspect.");

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            base.Process();

            if (!pursuing && thief.DistanceTo(Game.LocalPlayer.Character) < 10f && thief.IsOnScreen)
            {
                pursuing = true;
                pursuit = Functions.CreatePursuit();
                Functions.SetPursuitAsCalledIn(pursuit, false);
                Functions.AddPedToPursuit(pursuit, thief);
            }

            if (pursuing && (pursuit == null || !Functions.IsPursuitStillRunning(pursuit)))
            {
                EndSuccess();
                return;
            }

            if (!car) EndSuccess();
            if (!thief || thief.IsDead || Functions.IsPedArrested(thief)) EndSuccess();
        }

        public override void End()
        {
            if (thief && !Functions.IsPedArrested(thief)) thief.Dismiss();
            if (car) car.Dismiss();

            base.End();
        }
    }
}
