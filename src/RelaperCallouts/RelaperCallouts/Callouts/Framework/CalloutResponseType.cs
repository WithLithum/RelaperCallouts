﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

namespace RelaperCallouts.Callouts.Framework
{
    public enum CalloutResponseType
    {
        Code2,
        Code3,
        Code99
    }
}