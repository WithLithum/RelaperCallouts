﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using System;
using LSPD_First_Response.Mod.API;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Extern;
using RelaperCallouts.Util;
using RelaperCommons;
using RelaperCommons.Entities;

namespace RelaperCallouts.Callouts
{
    [CalloutInfo("Mugging", CalloutProbability.Medium)]
    public class Mugging : CalloutBase
    {
        private Ped robber;
        private Ped victim;
        private bool spooked;
        private LHandle pursuit;
        private bool tasking;

        protected override string Name => "Mugging";

        protected override string ScannerCrimeName => "MUGGING";
        protected override bool HasNormalCrimeAudio => true;
        protected override bool HasReportCrimeAudio => false;

        public override bool OnBeforeCalloutDisplayed()
        {
            if (!SpawnUtil.TryGenerateSpawnPointOnPedWalk(300f, 600f, false, out Vector3 outP))
            {
                Game.LogTrivial("Rel.C: Failed to find spawn point");
                return false;
            }

            SpawnPoint = outP;

            this.AddMinimumDistanceCheck(250f, SpawnPoint);
            this.AddMaximumDistanceCheck(650f, SpawnPoint);
            this.ResponseType = CalloutResponseType.Code3;
            this.ReportedByUnits = false;

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            if (ExternManager.ResponseReplaceInstalled)
            {
                robber = ResponseReplaceFunctions.CreatePed(SpawnPoint.Around(0.8f));
                robber.IsPersistent = true;
                robber.BlockPermanentEvents = true;
            }
            else
            {
                robber = new Ped(SpawnPoint.Around(0.8f))
                {
                    IsPersistent = true,
                    BlockPermanentEvents = true
                };
            }

            robber.Inventory.GiveNewWeapon(WeaponHash.Pistol, 40, true);

            if (ExternManager.ResponseReplaceInstalled)
            {
                victim = ResponseReplaceFunctions.CreatePed(SpawnPoint.Around(0.4f));
                victim.IsPersistent = true;
                victim.BlockPermanentEvents = true;
            }
            else
            {
                victim = new Ped(SpawnPoint.Around(0.4f))
                {
                    IsPersistent = true,
                    BlockPermanentEvents = true
                };
            }

            Blip = new Blip(robber.Position.Around(30f), 80f)
            {
                Alpha = 0.5f,
                IsRouteEnabled = true
            };
            Blip.SetColor(BlipColor.Yellow);

            ScannerMessages.DisplayDispatchText("Mugging", "Find the ~r~robber ~w~and save the ~g~victim~w~ in the ~y~area~w~!");
            Game.DisplaySubtitle("Go to ~y~" + World.GetStreetName(World.GetStreetHash(Game.LocalPlayer.Character.Position)) + ".");

            return base.OnCalloutAccepted();
        }

        public override void Process()
        {
            if (!robber.Exists() || robber.IsDead || Functions.IsPedArrested(robber)) EndSuccess();

            // When player is close, the suspect will now aim the victim.
            // This is observed and learned from Fighting call-out in United Call-outs.
            if (!tasking && Game.LocalPlayer.Character.DistanceTo(robber) < 50f && robber.IsOnScreen)
            {
                tasking = true;

                robber.Tasks.AimWeaponAt(victim, -1);
                victim.Tasks.PutHandsUp(-1, robber);
            }

            if (!spooked && Game.LocalPlayer.Character.DistanceTo2D(robber) < 5f && robber.IsOnScreen)
            {
                spooked = true;
                // Prevent cheating
                if (Blip) Blip.Delete();

                // 1/4 chance to shoot the victim
                if (MathHelper.GetRandomInteger(5) == 2)
                {
                    Game.DisplaySubtitle("Take out the ~r~robber~w~.");
                    Functions.SetPursuitDisableAIForPed(robber, true);
                    // Setting any ped to not block permanent events while they have a weapon
                    // will simply make them fight back
                    robber.BlockPermanentEvents = false;
                    robber.Tasks.FightAgainst(victim, -1);
                    // Victim don't have any weapon...
                    victim.BlockPermanentEvents = false;

                    Functions.PlayScannerAudioUsingPosition("RC_ATTENTION WE_HAVE CRIME_SHOTS_FIRED IN_OR_ON_POSITION RC_CODE3", Game.LocalPlayer.Character.Position);
                }
                else
                {
                    Game.DisplaySubtitle("Chase down the ~r~robber~w~.");
                    Functions.PlayScannerAudioUsingPosition("RC_ATTENTION WE_HAVE CRIME_RESIST_ARREST IN_OR_ON_POSITION RC_CODE3", Game.LocalPlayer.Character.Position);
                }

                // The victim will ran away as player will be close!
                victim.Tasks.ReactAndFlee(robber);
                victim.Dismiss();

                // There is no way to create a proper pursuit without setting
                // it to called in behavior.
                pursuit = Functions.CreatePursuit();
                Functions.AddPedToPursuit(pursuit, robber);
                Functions.SetPursuitAsCalledIn(pursuit, true);
                Functions.SetPursuitIsActiveForPlayer(pursuit, true);
            }

            if (spooked && !Functions.IsPursuitStillRunning(pursuit))
            {
                EndSuccess();
            }

            base.Process();
        }

        public override void End()
        {
            if (pursuit != null && Functions.IsPursuitStillRunning(pursuit))
            {
                Functions.ForceEndPursuit(pursuit);
            }

            if (ExternManager.ResponseReplaceInstalled)
            {
                ResponseReplaceFunctions.DismissPed(robber);
                ResponseReplaceFunctions.DismissPed(victim);
            }
            else
            {
                if (robber && !Functions.IsPedArrested(robber)) robber.Dismiss();
                if (victim && !Functions.IsPedArrested(victim)) victim.Dismiss();
            }
            if (victim)
            {
                if (victim.IsAlive && ExternManager.ResponseReplaceInstalled)
                {
                    try
                    {
                        ResponseReplaceFunctions.RegisterPed(victim);
                    }
#pragma warning disable CA1031 // Do not catch general exception types
                    catch (Exception ex)
                    {
                        Game.LogTrivial(ex.ToString());
                    }
#pragma warning restore CA1031 // Do not catch general exception types
                }
                if (!Functions.IsPedArrested(victim))
                {
                    victim.Dismiss();
                }
            }

            base.End();
        }
    }
}
