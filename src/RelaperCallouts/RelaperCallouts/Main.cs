﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

using LSPD_First_Response.Mod.API;
using Rage;
using RelaperCallouts.Callouts;
using RelaperCallouts.Callouts.Framework;
using RelaperCallouts.Extern;

namespace RelaperCallouts
{
    public class Main : Plugin
    {
        public override void Finally()
        {
        }

        public override void Initialize()
        {
            Functions.OnOnDutyStateChanged += Functions_OnOnDutyStateChanged;
        }

        private void Functions_OnOnDutyStateChanged(bool onDuty)
        {
            if (onDuty)
            {
                Game.LogTrivial("Rel.C: Registering callouts...");
                CalloutRegistry.Register(typeof(ArmoredCarRobbery));
                CalloutRegistry.Register(typeof(StolenVehicle));
                CalloutRegistry.Register(typeof(Mugging));
                CalloutRegistry.Register(typeof(FootPursuit));
                CalloutRegistry.Register(typeof(StolenEmergencyVehicle));
                CalloutRegistry.Register(typeof(DrivingUnderInfluence));
                CalloutRegistry.Register(typeof(Shootout));
                CalloutRegistry.Register(typeof(Fighting));

                Game.LogTrivial("Rel.C: Initializing engine...");
                Shootout.InitShooterGroup();
                ExternManager.Init();

                Game.DisplayNotification("web_lossantospolicedept", "web_lossantospolicedept", "RelaperCallouts", "~y~by RelaperCrystal", "~b~has been loaded. ~g~Enjoy!");

                Functions.OnOnDutyStateChanged -= Functions_OnOnDutyStateChanged;
                Game.LogTrivial("Rel.C: End initializing RelaperCallouts");
            }
        }
    }
}
