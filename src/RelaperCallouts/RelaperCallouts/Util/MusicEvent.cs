﻿// Copyright (C) RelaperCrystal 2021.
// Licensed under GNU GPL v3.

// As a special notice, you are permitted to copy the file freely without 
// following "copyleft" restrictions
// of the license, with or without modification, starting from line 14, with proper credits.

using Rage.Native;

namespace RelaperCallouts.Util
{
    public class MusicEvent
    {
        public MusicEvent(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public void Prepare()
        {
            NativeFunction.Natives.PREPARE_MUSIC_EVENT(Name);
        }

        public void Trigger()
        {
            NativeFunction.Natives.TRIGGER_MUSIC_EVENT(Name);
        }

        public void Dismiss()
        {
            NativeFunction.Natives.CANCEL_MUSIC_EVENT(Name);
        }
    }
}
